package com.lipro.netty.iso8583.server;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lipro.netty.util.MessageFactorySingleton;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Server {
	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	private final int port;
	
	public Server(int port) {
		this.port = port;
	}
	
	public static void main(String[] args) throws InterruptedException, IOException {
		new Server(Integer.getInteger("port") == null ? 8000 : Integer.getInteger("port"))
			.setup(args[0])
			.run();
	}
	
	public Server setup(String path) throws IOException {
		logger.debug("Setting Up Server with config at " + path);
		MessageFactorySingleton.getInstance(path);
		logger.debug("Setting Up Server at port " + this.port);
		return this;
	}
	
	public void run() throws InterruptedException {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		
		try {
			ServerBootstrap bootstrap = new ServerBootstrap()
					.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(new ServerInitializer());
			logger.debug("Server Started!");
			bootstrap.bind(this.port).sync().channel().closeFuture().sync();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
			logger.debug("Server Stopped!");
		}
	}
	
}
