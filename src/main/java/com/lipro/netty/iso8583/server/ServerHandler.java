package com.lipro.netty.iso8583.server;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lipro.netty.util.MessageFactorySingleton;
import com.solab.iso8583.IsoMessage;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundMessageHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;

public class ServerHandler extends ChannelInboundMessageHandlerAdapter<String> {
	
	private static final Logger logger = LoggerFactory.getLogger(ServerHandler.class);
	private static final ChannelGroup channels = new DefaultChannelGroup();
	
	@Override
	public void handlerAdded(ChannelHandlerContext ctx) {
		Channel incoming = ctx.channel();
		logger.info("[SERVER] - " + incoming.remoteAddress() + " has joined!");
		channels.add(incoming);
	}
	
	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) {
		Channel incoming = ctx.channel();
		logger.info("[SERVER] - " + incoming.remoteAddress() + " has left!");
		channels.remove(incoming);
	}
	
	@Override
	public void messageReceived(ChannelHandlerContext ctx, String msg) throws Exception {
		Channel incoming = ctx.channel();
		IsoMessage request = null;
		try {
			request = MessageFactorySingleton.getMessageFactory().parseMessage(msg.getBytes(), 0);
			logger.debug("Received message [{}]", msg);
			IsoMessage isoMsg = MessageFactorySingleton.getMessageFactory().newMessage(request.getType() + 0x010);
			fillIsoMsgResponse(isoMsg, request);
			String response = new String(isoMsg.writeData());
			logger.debug("Sent Response [" + print(isoMsg) +"] for Request [" + print(request) + "] remote address " + incoming.remoteAddress());
			incoming.write(response + "\n");
		} catch (Exception e) {
			logger.info("Incoming Request [{}]", msg);
			logger.error("Exception on processing incoming request: {}", e.getMessage());
			incoming.write("Invalid" + "\n");
		}
	}

	private static String print(IsoMessage m) {
		List<String> listValue = new ArrayList<>();
		listValue.add("Type: " + Integer.toHexString(m.getType()));
		for (int i = 2; i < 128; i++) {
			if (m.hasField(i)) {
				listValue.add("DE-" + i + " (" + m.getField(i).getType() + "): " + m.getField(i).toString());
			}
		}
		return listValue.toString();
	}
	
	private static void fillIsoMsgResponse(IsoMessage isoMsg, IsoMessage request) {
		for (int i = 2; i < 128; i++) {
			if (isoMsg.hasField(i)) {
				if (isoMsg.getObjectValue(i).toString().startsWith("#REQ")) {
					String resValue = isoMsg.getObjectValue(i).toString();
					int copyFromField = Integer.valueOf(resValue.substring(4, resValue.length()));
					isoMsg.setValue(i, request.getObjectValue(copyFromField), isoMsg.getField(i).getType(), request.getObjectValue(copyFromField).toString().length());
				}
			}
		}
	}

}
