package com.lipro.netty.util;

import java.io.IOException;
import java.net.URL;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.impl.SimpleTraceGenerator;
import com.solab.iso8583.parse.ConfigParser;

public final class MessageFactorySingleton {
	
	private static MessageFactorySingleton INSTANCE;
	private static MessageFactory<IsoMessage> messageFactory;
	private static String isoConfigPath;
	
	private MessageFactorySingleton(String path) {
		MessageFactorySingleton.isoConfigPath = path;
	}
	
	public static MessageFactorySingleton getInstance(String path) throws IOException {
		if (INSTANCE == null) {
			INSTANCE = new MessageFactorySingleton(path);
			URL url = new URL(isoConfigPath);
			messageFactory = ConfigParser.createFromUrl(url);
			messageFactory.setAssignDate(true);
			messageFactory.setTraceNumberGenerator(new SimpleTraceGenerator(1));
		}
		return INSTANCE;
	}
	
	public static MessageFactory<IsoMessage> getMessageFactory() {
		return messageFactory;
	}
	
}
